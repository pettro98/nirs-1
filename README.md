# ссылки

## общие методы
1. курс: алгоритмы и анализ сложности - (http://edu.mmcs.sfedu.ru/course/view.php?id=230)
1. ~~СПОСОБ ПОРОЖДЕНИЯ ЭВРИСТИК ДЛЯ РЕШЕНИЯ NP-ТРУДНЫХ ЗАДАЧ - (https://www.elibrary.ru/item.asp?id=12806202&)~~
1. Алгоритмизация труднорешаемых задач. Часть I. простые примеры и простые эвристики - (https://cyberleninka.ru/article/n/algoritmizatsiya-trudnoreshaemyh-zadach-chast-i-prostye-primery-i-prostye-evristiki)
1. Алгоритмизация труднорешаемых задач. Часть II. Более сложные эвристики - (https://cyberleninka.ru/article/n/algoritmizatsiya-trudnoreshaemyh-zadach-chast-ii-bolee-slozhnye-evristiki)
1. ~~Алгоритмизация труднорешаемых задач. Часть III.~~

## методы для конкретных задач
### комбинированные и другие методы
1. ЗАДАЧА О ПОКРЫТИИ МНОЖЕСТВА: СЛОЖНОСТЬ, АЛГОРИТМЫ, ЭКСПЕРИМЕНТАЛЬНЫЕ ИССЛЕДОВАНИЯ - (https://www.researchgate.net/profile/Lidia-Zaozerskaya/publication/268251266_The_set_covering_problem_Complexity_algorithms_experiments/links/5f5c79fc4585154dbbcb38df/The-set-covering-problem-Complexity-algorithms-experiments.pdf)
1. ПОВЕДЕНИЕ ВЕРОЯТНОСТНЫХ ЖАДНЫХ АЛГОРИТМОВ ДЛЯ МНОГОСТАДИЙНОЙ ЗАДАЧИ РАЗМЕЩЕНИЯ - (http://math.nsc.ru/publishing/DAOR/content/1999/05/12.pdf)
1. ТЕОРИЯ И ПРАКТИКА РЕШЕНИЯ NP-ТРУДНОЙ ЗАДАЧИ СТРУКТУРНОГО СИНТЕЗА ПРОГРАММНЫМИ СРЕДСТВАМИ - (https://www.elibrary.ru/item.asp?id=12830712)
1. Разработка гибридного алгоритма решения задачи упорядочения - (https://cyberleninka.ru/article/n/razrabotka-gibridnogo-algoritma-resheniya-zadachi-uporyadocheniya)

### динамическое программирование
1. ТОЧНЫЙ АЛГОРИТМ РЕШЕНИЯ ДИСКРЕТНОЙ ЗАДАЧИ ВЕБЕРА ДЛЯ k-ДЕРЕВА - (http://math.nsc.ru/publishing/DAOR/content/2014/03/416.pdf)

### метод ветвей и границ
1. Алгоритм ветвей и границ для задачи составления расписания на параллельных процессорах - (https://cyberleninka.ru/article/n/algoritm-vetvey-i-granits-dlya-zadachi-sostavleniya-raspisaniya-na-parallelnyh-protsessorah)
1. Эффективная реализация алгоритма решения задачи коммивояжёра методом ветвей и границ - (https://cyberleninka.ru/article/n/effektivnaya-realizatsiya-algoritma-resheniya-zadachi-kommivoyazhyora-metodom-vetvey-i-granits)

### генетические/эволюционные алгоритмы
1. Применение генетических алгоритмов к решению задач дискретной оптимизации - (http://www.unn.ru/pages/issues/aids/2007/15.pdf)
1. Генетический алгоритм решения задачи отображения графа - (https://cyberleninka.ru/article/n/geneticheskiy-algoritm-resheniya-zadachi-otobrazheniya-grafa)
1. Модификация вероятностного генетического алгоритма решения задачи размещения элементов Эва с учетом электромагнитной совместимости - (https://cyberleninka.ru/article/n/modifikatsiya-veroyatnostnogo-geneticheskogo-algoritma-resheniya-zadachi-razmescheniya-elementov-eva-s-uchetom-elektromagnitnoy)
1. Гибридный подход для решения задачи 3-х мерной упаковки - (https://cyberleninka.ru/article/n/gibridnyy-podhod-dlya-resheniya-zadachi-3-h-mernoy-upakovki)
1. Двухуровневый алгоритм разбиения графа на части - (https://cyberleninka.ru/article/n/dvuhurovnevyy-algoritm-razbieniya-grafa-na-chasti)
1. Интегрированный алгоритм размещения фрагментов cбис - (https://cyberleninka.ru/article/n/integrirovannyy-algoritm-razmescheniya-fragmentov-cbis)
1. Программный комплекс решения задачи кластеризации - (https://cyberleninka.ru/article/n/programmnyy-kompleks-resheniya-zadachi-klasterizatsii)
1. Эволюционный метод решения задачи группы квадрокоптеров для повышения качества мониторинга области - (https://cyberleninka.ru/article/n/evolyutsionnyy-metod-resheniya-zadachi-gruppy-kvadrokopterov-dlya-povysheniya-kachestva-monitoringa-oblasti)

